// function createCard(name, description, pictureUrl) {
//     return `
//       <div class="card">
//         <img src="{pictureUrl}" class="card-img-top">
//         <div class="card-body">
//           <h5 class="card-title">{name}</h5>
//           <p class="card-text">{description}</p>
//         </div>
//       </div>
//     `;
//   }

// window.addEventListener("DOMContentLoaded", async () => {
//   const url = "http://localhost:8000/api/conferences/";

//   try {
//     const response = await fetch(url);

//     if (!response.ok) {
//       // Figure out what to do when the response is bad
//     } else {
//       const data = await response.json();

//       const conference = data.conferences[0];
//       const nameTag = document.querySelector(".card-title");
//       nameTag.innerHTML = conference.name;

//       const detailUrl = `http://localhost:8000${conference.href}`;
//       const detailResponse = await fetch(detailUrl);
//       if (detailResponse.ok) {
//         const details = await detailResponse.json();
//         const descriptionTag = document.querySelector(".card-text");
//         console.log(details.conference.description);
//         descriptionTag.innerHTML = details.conference.description;

//         const imageTag = document.querySelector(".card-img-top");
//         imageTag.src = details.conference.location.picture_url;
//         console.log(details);
//       }
//     }
//   } catch (e) {
//     // Figure out what to do if an error is raised
//   }
// });
function alert(wow){
    return `
    <div class="alert alert-danger" role="alert">
    ${wow}
    </div
    `
}
function createCard(name, description, pictureUrl, starts, ends, location) {
  return `
    <div class="card m-3">
        <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        ${new Date(starts).toLocaleDateString()} -
        ${new Date(ends).toLocaleDateString()}
        </div>
    </div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";

  try {
    const response = await fetch(url);

    if (!response.ok) {
      const wow = "Get outta here!"
      const notice = document.querySelector(`main`);
      notice.innerHTML = alert(wow);
      
    } else {
      const data = await response.json();

      let index = 0;
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const title = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const location = details.conference.location.name;
          const starts = details.conference.starts;
          const ends = details.conference.ends;
          const html = createCard(title, description, pictureUrl, starts, ends, location);
          const column = document.querySelector(`#col-${index}`);
          column.innerHTML += html;
          index += 1;
          if (index > 2) {
            index = 0;
          }
        }
      }
    }
  } catch (e) {
    console.error(e);
  }
});

window.addEventListener("DOMContentLoaded", async () => {
    const url = "http://localhost:8000/api/conferences/";
    try {
      const response = await fetch(url);
  
      if (response.ok) {
          const data = await response.json();
          console.log(data);
  
          // Get the select tag element by its id 'state'
          const selectTag = document.getElementById("conference");
          // For each state in the states property of the data
          for (let conference of data.conferences) {
          // Create an 'option' element
          const option = document.createElement("option");
          // Set the '.value' property of the option element to the
          // state's abbreviation
          option.value = conference.href;
          // Set the '.innerHTML' property of the option element to
          // the state's name
          option.innerHTML = conference.name;
          // Append the option element as a child of the select tag
          selectTag.appendChild(option);
          }
      }
  
      const formTag = document.getElementById("create-presentation-form");
      formTag.addEventListener("submit", async (event) => {
          event.preventDefault();
  
          const formData = new FormData(formTag);
          const object = Object.fromEntries(formData);
          const json = JSON.stringify(object);
          const presentationUrl = `http://localhost:8000${object.conference}presentations/`;
          const fetchConfig = {
          method: "post",
          body: json,
          headers: {
              "Content-Type": "application/json",
          },
          };
          const response = await fetch(presentationUrl, fetchConfig);
          if (response.ok) {
          formTag.reset();
          const newPresentation = await response.json();
          console.log(newPresentation);
          }
          });
      } catch (e) {
          console.error(e)
      }
  });